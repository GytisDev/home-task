package com.gytisdev.currencyexchanger.di.modules

import android.content.Context
import com.gytisdev.currencyexchanger.database.AppDatabase
import com.gytisdev.currencyexchanger.database.daos.BalanceDao
import com.gytisdev.currencyexchanger.database.daos.HistoryDao
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Singleton

/**
 * Provides database-related classes like database or DAOs
 */
@Module
object DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(applicationContext: Context): AppDatabase {
        return AppDatabase.getInstance(applicationContext)
    }

    @Provides
    @Reusable
    fun provideBalanceDao(database: AppDatabase): BalanceDao {
        return database.balanceDao
    }

    @Provides
    @Reusable
    fun provideHistoryDao(database: AppDatabase): HistoryDao {
        return database.historyDao
    }
}