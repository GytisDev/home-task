package com.gytisdev.currencyexchanger.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Lazy
import javax.inject.Inject

/**
 * Enables to easily inject into view model classes with Dagger
 * @author matejdro
 * @see https://github.com/google/dagger/issues/1273#issuecomment-447997439
 */
class ViewModelFactory<T: ViewModel> @Inject constructor(private val viewModel: Lazy<T>) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T = viewModel.get() as T
}