package com.gytisdev.currencyexchanger.di

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.gytisdev.currencyexchanger.exchanger.ExchangerActivity
import com.gytisdev.currencyexchanger.di.modules.*
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ContextModule::class, DatabaseModule::class, NetworkModule::class])
interface AppComponent {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: Application): AppComponent
    }

    fun provideApplication(): Application
    fun provideApplicationContext(): Context
    fun provideResources(): Resources

    // Activities
    fun inject(target: ExchangerActivity)
}