package com.gytisdev.currencyexchanger.di.modules

import android.app.Application
import android.content.Context
import android.content.res.Resources
import dagger.Module
import dagger.Provides
import dagger.Reusable

/**
 * Provides context-related android classes
 */
@Module
object ContextModule {
    @Reusable
    @Provides
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }
    @Reusable
    @Provides
    fun provideResources(applicationContext: Context) : Resources {
        return applicationContext.resources
    }
}