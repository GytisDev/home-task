package com.gytisdev.currencyexchanger.di

import com.gytisdev.currencyexchanger.ExchangerApplication
import com.gytisdev.currencyexchanger.exchanger.ExchangerActivity

object Injector {
    val appComponent: AppComponent by lazy {
        ExchangerApplication.instance.appComponent
    }
}

fun ExchangerActivity.inject() {
    Injector.appComponent.inject(this)
}