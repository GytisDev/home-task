package com.gytisdev.currencyexchanger.network.models

import com.gytisdev.currencyexchanger.exchanger.data.models.Rates
import java.math.BigDecimal

/**
 * Class that maps to rates remote API response
 */
data class NetworkRateResponse (
    val rates: Map<String, Double>,
    val base: String,
    val date: String
)

/**
 * Converts the network model to domain model to be used by the application
 * @return domain model rates
 */
fun NetworkRateResponse.toDomainModel() : Rates {
    val temp = rates.entries.associate { it.key to it.value.toBigDecimal() }.toMutableMap()
    temp[base] = BigDecimal.ONE
    return Rates(temp, base)
}