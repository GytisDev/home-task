package com.gytisdev.currencyexchanger.network

import com.gytisdev.currencyexchanger.network.models.NetworkRateResponse
import retrofit2.http.GET

/**
 * Used to create the API service retrofit client
 */
interface ApiService {
    /**
     * @return newest rates from remote API mapped to network model
     */
    @GET("latest")
    suspend fun getRates() : NetworkRateResponse
}