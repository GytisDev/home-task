package com.gytisdev.currencyexchanger.extensions

import com.gytisdev.currencyexchanger.constants.CalculationsConstants
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Converts [BigDecimal] to a form with 2 decimal places
 * @return a new [BigDecimal] that has 2 decimal places
 */
fun BigDecimal.toRepresentationalScale() : BigDecimal =
    this.setScale(CalculationsConstants.REPRESENTATION_SCALE, RoundingMode.HALF_UP)