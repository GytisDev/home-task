package com.gytisdev.currencyexchanger.extensions

import android.app.Activity
import android.app.AlertDialog
import com.gytisdev.currencyexchanger.exchanger.data.models.AlertDialogData

/**
 * Helper extension method to not clutter activity class
 * Opens an alert dialog inside of an activity and sets it's fields
 * @param alertDialogData data that the dialog will show
 */
fun Activity.showAlertDialog(alertDialogData: AlertDialogData) {
    val alertDialog: AlertDialog = AlertDialog.Builder(this).create()
    alertDialog.setTitle(alertDialogData.title)
    alertDialog.setMessage(alertDialogData.message)
    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, alertDialogData.buttonText) { dialog, _ ->
        dialog.dismiss()
    }
    alertDialog.show()
}