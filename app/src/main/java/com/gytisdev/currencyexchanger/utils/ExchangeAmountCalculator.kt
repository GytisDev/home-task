package com.gytisdev.currencyexchanger.utils

import com.gytisdev.currencyexchanger.constants.CalculationsConstants
import com.gytisdev.currencyexchanger.exchanger.data.ConversionData
import com.gytisdev.currencyexchanger.exchanger.data.rules.RulesProvider
import com.gytisdev.currencyexchanger.extensions.toRepresentationalScale
import java.math.BigDecimal
import java.math.RoundingMode
import javax.inject.Inject

/**
 * Responsible for various calculations related to currency amounts
 */
class ExchangeAmountCalculator @Inject constructor(private val rules: RulesProvider) {
    /**
     * Calculates the expected 'to' amount based on the 'from' and 'to' base-currency rates
     * @param amount - amount that will be converted
     * @param rateFrom - base currency from rate (e.g. USD -> EUR when EUR is base)
     * @param rateTo - base currency to rate (e.g. EUR -> USD when EUR is base)
     * @return converted amount from 'from' to 'to' currencies
     */
    fun calculateExpectedAmount(amount: BigDecimal, rateFrom: BigDecimal, rateTo: BigDecimal) : BigDecimal {
        val multiplyResult = amount.divide(rateFrom, CalculationsConstants.CALCULATIONS_SCALE, RoundingMode.HALF_UP)
        return multiplyResult.multiply(rateTo).setScale(CalculationsConstants.CALCULATIONS_SCALE, RoundingMode.HALF_UP)
    }

    /**
     * Calculates the expected 'to' amount based on the 'from' and 'to' base-currency rates
     * @param amount - amount that will be converted
     * @param rateFrom - base currency from rate (e.g. USD -> EUR when EUR is base)
     * @param rateTo - base currency to rate (e.g. EUR -> USD when EUR is base)
     * @return converted amount from 'to' to 'from' currencies
     */
    fun calculateRequiredAmount(amount: BigDecimal, rateFrom: BigDecimal, rateTo: BigDecimal) : BigDecimal {
        val divideResult = rateFrom.divide(rateTo, CalculationsConstants.CALCULATIONS_SCALE, RoundingMode.HALF_UP)
        return amount.multiply(divideResult).setScale(CalculationsConstants.CALCULATIONS_SCALE, RoundingMode.HALF_UP)
    }

    /**
     * Calculates the commission fee based on currently added rules
     * @param data - data about the new conversion
     * @return the commission fee
     */
    fun calculateCommissionsFee(data: ConversionData): BigDecimal {
        var commissionFee = BigDecimal.ONE

        rules.activeRules.forEach {
            val ruleCommissionFee = it.calculateCommissionsFee(data)
            if(ruleCommissionFee < commissionFee) {
                commissionFee = ruleCommissionFee
            }
        }

        return data.conversionAmount.multiply(commissionFee).toRepresentationalScale()
    }
}