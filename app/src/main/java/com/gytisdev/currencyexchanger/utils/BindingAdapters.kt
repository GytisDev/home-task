package com.gytisdev.currencyexchanger.utils

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gytisdev.currencyexchanger.constants.CalculationsConstants
import com.gytisdev.currencyexchanger.exchanger.data.BalanceAdapter
import com.gytisdev.currencyexchanger.exchanger.data.models.Balance
import java.math.RoundingMode

@BindingAdapter("balance")
fun setBalanceInfo(view: TextView, balance: Balance) {
    val amountRounded = balance.amount.setScale(CalculationsConstants.REPRESENTATION_SCALE, RoundingMode.HALF_UP)
    view.text = "${amountRounded} ${balance.currency}"
}

@BindingAdapter("balancesData")
fun setBalancesListData(view: RecyclerView, data: List<Balance>?) {
    val adapter = view.adapter as BalanceAdapter
    adapter.submitList(data)
}