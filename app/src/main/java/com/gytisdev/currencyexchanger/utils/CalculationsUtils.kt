package com.gytisdev.currencyexchanger.utils

import java.math.BigDecimal

object CalculationsUtils {
    /**
     * Calculates the new balance after subtracting an amount
     * @param balances map of balances with (currency, amount) pairs
     * @param currency the currency which balance will be subtracted
     * @param amountToAdd the amount that will be subtracted
     * @return the new balance
     */
    fun calculateNewBalanceSubtract(balances: Map<String, BigDecimal>, currency: String, amountToAdd: BigDecimal) =
        calculateNewBalance(balances, currency, -amountToAdd)

    /**
     * Calculates the new balance after adding an amount
     * @param balances map of balances with (currency, amount) pairs
     * @param currency the currency which balance will be added
     * @param amountToAdd the amount that will be added
     * @return the new balance
     */
    fun calculateNewBalance(balances: Map<String, BigDecimal>, currency: String, amountToAdd: BigDecimal): BigDecimal {
        val amountInBalance = balances[currency] ?: BigDecimal.ZERO
        return amountInBalance.add(amountToAdd)
    }
}