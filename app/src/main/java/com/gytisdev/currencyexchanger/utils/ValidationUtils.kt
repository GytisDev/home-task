package com.gytisdev.currencyexchanger.utils

import com.gytisdev.currencyexchanger.exchanger.data.UserPreferencesContainer
import java.math.BigDecimal

/**
 * Methods for validating if conversion can continue with the provided data
 */
object ValidationUtils {
    /**
     * Validates if the data is not empty
     * @param userPreferencesContainer - data to be validated
     * @return indicator whether or not the data is not empty
     */
    fun validateUserPreferences(userPreferencesContainer: UserPreferencesContainer): Boolean {
        return userPreferencesContainer.fromAmount > BigDecimal.ZERO
                    && userPreferencesContainer.fromCurrency.isNotBlank()
                    && userPreferencesContainer.toCurrency.isNotBlank()
    }

    /**
     * Validates if the currencies are not the same
     * @param userPreferencesContainer - data to be validated
     * @return indicator whether or not the currencies are same
     */
    fun validateCurrency(userPreferencesContainer: UserPreferencesContainer): Boolean {
        return userPreferencesContainer.fromCurrency.compareTo(userPreferencesContainer.toCurrency) != 0
    }

    /**
     * Checks if balance is too low to perform a conversion
     * @param accountBalance - current balance
     * @param amountToConvert - amount that will be converted from the balance
     * @param commissions - commissions amount that will also be subtracted from the balance
     * @return indicator whether or not the balance is sufficient
     */
    fun isBalanceTooLow(
        accountBalance: BigDecimal?,
        amountToConvert: BigDecimal,
        commissions: BigDecimal
    ): Boolean {
        val balance = accountBalance ?: BigDecimal.ZERO
        return balance < amountToConvert.add(commissions)
    }
}