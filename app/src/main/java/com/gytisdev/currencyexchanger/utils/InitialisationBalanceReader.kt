package com.gytisdev.currencyexchanger.utils

import android.content.Context
import com.google.gson.Gson
import com.gytisdev.currencyexchanger.database.entities.BalanceEntity
import timber.log.Timber
import java.io.InputStreamReader
import java.lang.Exception

class InitialisationBalanceReader {
    companion object {
        private const val INITIALISATION_FILE_NAME = "config/balance.json"

        /**
         * Reads a json file and returns the read result
         * Used for pre-populating the local database
         * @return list of balance entities that were read, empty list if reading was unsuccessful
         */
        fun readJsonInitialisationFile(context: Context): List<BalanceEntity> {
            return try {
                val assetsManager = context.assets
                val fileStream = assetsManager.open(INITIALISATION_FILE_NAME)
                Gson().fromJson(InputStreamReader(fileStream, "UTF-8"), Array<BalanceEntity>::class.java).toList()
            } catch (e: Exception) {
                Timber.e(e)
                emptyList()
            }
        }
    }
}