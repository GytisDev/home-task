package com.gytisdev.currencyexchanger

import android.app.Application
import com.gytisdev.currencyexchanger.di.AppComponent
import com.gytisdev.currencyexchanger.di.DaggerAppComponent
import timber.log.Timber

class ExchangerApplication : Application() {
    val appComponent: AppComponent by lazy {
        DaggerAppComponent
            .factory()
            .create(this)
    }

    override fun onCreate() {
        instance = this
        if(BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        super.onCreate()
    }

    companion object {
        lateinit var instance : ExchangerApplication
            private set
    }
}