package com.gytisdev.currencyexchanger.constants.errors

interface Error {
    fun messageId(): Int
}