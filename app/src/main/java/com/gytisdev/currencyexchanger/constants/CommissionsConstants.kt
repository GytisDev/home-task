package com.gytisdev.currencyexchanger.constants

import java.math.BigDecimal

object CommissionsConstants {
    val DEFAULT_COMMISSION_FEE_PERCENT = BigDecimal("0.007") // 0.7%
}