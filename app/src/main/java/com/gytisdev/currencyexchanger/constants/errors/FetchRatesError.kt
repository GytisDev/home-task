package com.gytisdev.currencyexchanger.constants.errors

import androidx.annotation.StringRes
import com.gytisdev.currencyexchanger.R

enum class FetchRatesError(@StringRes val messageId: Int): Error {
    NO_CONNECTION(R.string.message_error_connection),
    OUT_OF_SYNC(R.string.message_error_out_of_sync);

    override fun messageId(): Int {
        return messageId
    }
}