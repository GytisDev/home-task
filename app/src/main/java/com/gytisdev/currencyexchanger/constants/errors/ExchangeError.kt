package com.gytisdev.currencyexchanger.constants.errors

import androidx.annotation.StringRes
import com.gytisdev.currencyexchanger.R

enum class ExchangeError(@StringRes val messageId: Int) : Error {
    EMPTY_DATA(R.string.message_error_data),
    INSUFFICIENT_FUNDS(R.string.message_error_funds),
    CURRENCIES_MATCH(R.string.message_error_currencies_match);

    override fun messageId(): Int {
        return messageId
    }
}