package com.gytisdev.currencyexchanger.constants

object NetworkConstants {
    const val RATES_FETCH_PERIOD: Long = 5 * 1000 // 5 seconds
}