package com.gytisdev.currencyexchanger.constants

object CalculationsConstants {
    const val REPRESENTATION_SCALE = 2
    const val CALCULATIONS_SCALE = 10
}