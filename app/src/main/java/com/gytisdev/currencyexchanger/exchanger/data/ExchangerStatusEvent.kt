package com.gytisdev.currencyexchanger.exchanger.data

import com.gytisdev.currencyexchanger.constants.errors.Error

sealed class ExchangerStatusEvent {
    data class ExchangeSuccess(val from: String, val to: String, val commissionFee: String) : ExchangerStatusEvent()
    data class Error(val error: com.gytisdev.currencyexchanger.constants.errors.Error) : ExchangerStatusEvent()
}
