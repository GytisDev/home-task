package com.gytisdev.currencyexchanger.exchanger.data.models

data class AlertDialogData (
    val title: String,
    val message: String,
    val buttonText: String
)