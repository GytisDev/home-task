package com.gytisdev.currencyexchanger.exchanger.data.rules.rulesimpl

import com.gytisdev.currencyexchanger.constants.CommissionsConstants
import com.gytisdev.currencyexchanger.exchanger.data.ConversionData
import com.gytisdev.currencyexchanger.exchanger.data.rules.Rule
import java.math.BigDecimal

class DefaultRule : Rule {
    override fun calculateCommissionsFee(ruleData: ConversionData): BigDecimal {
        return CommissionsConstants.DEFAULT_COMMISSION_FEE_PERCENT
    }
}