package com.gytisdev.currencyexchanger.exchanger.data.rules.rulesimpl

import com.gytisdev.currencyexchanger.constants.CommissionsConstants
import com.gytisdev.currencyexchanger.exchanger.data.ConversionData
import com.gytisdev.currencyexchanger.exchanger.data.rules.Rule
import java.math.BigDecimal

class FirstFiveFreeRule : Rule {
    override fun calculateCommissionsFee(ruleData: ConversionData): BigDecimal {
        if(ruleData.conversionCount <= Constants.COUNT) {
            return Constants.COMMISSION_FEE
        }

        return CommissionsConstants.DEFAULT_COMMISSION_FEE_PERCENT
    }

    private object Constants {
        const val COUNT = 5
        val COMMISSION_FEE: BigDecimal = BigDecimal.ZERO
    }
}