package com.gytisdev.currencyexchanger.exchanger.data

import java.math.BigDecimal

data class UserPreferencesContainer (
    var fromCurrency: String = "",
    var toCurrency: String = "",
    var fromAmount: BigDecimal = BigDecimal.ZERO,
    var toAmount: BigDecimal = BigDecimal.ZERO,
    var selectedField: SelectedField = SelectedField.None
)