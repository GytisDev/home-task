package com.gytisdev.currencyexchanger.exchanger.data

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gytisdev.currencyexchanger.R
import com.gytisdev.currencyexchanger.databinding.ItemBalanceListBinding
import com.gytisdev.currencyexchanger.exchanger.data.models.Balance

class BalanceAdapter : ListAdapter<Balance, BalanceAdapter.BalanceViewHolder>(DiffCallback) {
    class BalanceViewHolder(private val binding: ItemBalanceListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(balance: Balance) {
            binding.balance = balance
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BalanceViewHolder {
        val dataBinding: ItemBalanceListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_balance_list,
            parent,
            false
        )

        return BalanceViewHolder(dataBinding)
    }

    override fun onBindViewHolder(holder: BalanceViewHolder, position: Int) {
        val balance = getItem(position)
        holder.bind(balance)
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Balance>() {
        override fun areItemsTheSame(oldItem: Balance, newItem: Balance): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Balance, newItem: Balance): Boolean {
            return oldItem == newItem
        }
    }


}