package com.gytisdev.currencyexchanger.exchanger.data

import java.math.BigDecimal
import java.util.*

data class ConversionData (
    val conversionCount: Int,
    val conversionAmount: BigDecimal,
    val conversionAmountInBase: BigDecimal,
    val conversionCurrency: String,
    val conversionDate: Date
)