package com.gytisdev.currencyexchanger.exchanger.data.models

import java.math.BigDecimal

data class Balance (
    val currency: String,
    val amount: BigDecimal
)

fun List<Balance>.toMap() : Map<String, BigDecimal> =
    this.associate { it.currency to it.amount }