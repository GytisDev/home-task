package com.gytisdev.currencyexchanger.exchanger

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.gytisdev.currencyexchanger.constants.NetworkConstants
import com.gytisdev.currencyexchanger.constants.Outcome
import com.gytisdev.currencyexchanger.constants.errors.FetchRatesError
import com.gytisdev.currencyexchanger.database.daos.BalanceDao
import com.gytisdev.currencyexchanger.database.entities.BalanceEntity
import com.gytisdev.currencyexchanger.network.models.NetworkRateResponse
import com.gytisdev.currencyexchanger.database.entities.toDomainModel
import com.gytisdev.currencyexchanger.network.ApiService
import com.gytisdev.currencyexchanger.network.models.toDomainModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import java.io.IOException
import java.math.BigDecimal
import javax.inject.Inject

/**
 * Repository responsible for fetching newest rates and manipulating current balance
 */
class ExchangerRepository @Inject constructor(private val apiService: ApiService,
                                              private val balanceDao: BalanceDao) {
    /**
     * All balances that are currently in local database
     * @return live data that holds balances
     */
    val balance = Transformations.map(balanceDao.getBalance()) {
        it.toDomainModel()
    }

    /**
     * Flow that emits newest rates from remote API
     * Repeats periodically
     * @return [Outcome] object, which indicates whether or not the fetch was successful
     */
    fun ratesFlow() = flow {
        while(true) {
            try {
                val response = apiService.getRates()
                emit(Outcome.Success(response))
            }
            catch (e: IOException) {
                emit(Outcome.Failure(FetchRatesError.NO_CONNECTION))
            }
            delay(NetworkConstants.RATES_FETCH_PERIOD)
        }
    }

    /**
     * Updates specific balance in the local database. Inserts if it doesn't exist
     * @param currency the currency that will hold the new amount
     * @param amount amount that will be saved
     */
    suspend fun updateBalance(currency: String, amount: BigDecimal) {
        withContext(Dispatchers.IO) {
            balanceDao.insertBalance(
                BalanceEntity(
                    currency,
                    amount
                )
            )
        }
    }

    /**
     * Removes the specified currency from the database
     * @param currency the currency which's balance will be removed
     */
    suspend fun removeBalance(currency: String) {
        withContext(Dispatchers.IO) {
            balanceDao.deleteByCurrency(currency)
        }
    }


}