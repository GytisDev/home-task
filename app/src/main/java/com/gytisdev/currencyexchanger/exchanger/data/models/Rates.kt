package com.gytisdev.currencyexchanger.exchanger.data.models

import java.math.BigDecimal

data class Rates (
    val ratesMap: Map<String, BigDecimal>,
    val base: String
) {
    companion object {
        val Empty : Rates
            get() = Rates(
                emptyMap(),
                ""
            )
    }
}

fun Rates.toCurrencyCodesList() : List<String> {
    val list = mutableListOf<String>()
    this.ratesMap.forEach() {
        list.add(it.key)
    }
    list.remove(base)
    list[0] = base
    return list
}