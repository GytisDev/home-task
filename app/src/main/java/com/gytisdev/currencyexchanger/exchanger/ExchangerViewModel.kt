package com.gytisdev.currencyexchanger.exchanger

import androidx.lifecycle.*
import com.gytisdev.currencyexchanger.constants.Outcome
import com.gytisdev.currencyexchanger.constants.errors.ExchangeError
import com.gytisdev.currencyexchanger.constants.errors.FetchRatesError
import com.gytisdev.currencyexchanger.exchanger.data.*
import com.gytisdev.currencyexchanger.extensions.toRepresentationalScale
import com.gytisdev.currencyexchanger.exchanger.data.models.*
import com.gytisdev.currencyexchanger.network.models.NetworkRateResponse
import com.gytisdev.currencyexchanger.network.models.toDomainModel
import com.gytisdev.currencyexchanger.utils.CalculationsUtils
import com.gytisdev.currencyexchanger.utils.ExchangeAmountCalculator
import com.gytisdev.currencyexchanger.utils.ValidationUtils
import com.hadilq.liveevent.LiveEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

/**
 * View model responsible for business logic of the exchanger window
 */
class ExchangerViewModel @Inject constructor(
    private val exchangerRepository: ExchangerRepository,
    private val exchangerHistoryRepository: ExchangerHistoryRepository,
    private val exchangeAmountCalculator: ExchangeAmountCalculator
) : ViewModel() {

    private val _statusEvent = LiveEvent<ExchangerStatusEvent>()
    val statusEvent: LiveData<ExchangerStatusEvent> = _statusEvent

    private val _fromAmount = MutableLiveData<BigDecimal>()
    val fromAmount: LiveData<BigDecimal> = _fromAmount

    private val _toAmount = MutableLiveData<BigDecimal>()
    val toAmount: LiveData<BigDecimal> = _toAmount

    private val _exchangeRates = MutableLiveData<List<String>>()
    val exchangeRates : LiveData<List<String>> = _exchangeRates

    private var rates: Rates = Rates.Empty
    private lateinit var balances: Map<String, BigDecimal>
    private val userPreferences = UserPreferencesContainer()
    private var isRatesInSync = true

    /**
     * Balance values that the user will see
     */
    val balance = Transformations.map(exchangerRepository.balance) {
        balances = it.toMap()
        it
    }

    /**
     * Instructs the repository to start fetching newest rates periodically
     */
    fun initialize() {
        viewModelScope.launch {
            exchangerRepository.ratesFlow().collect { outcome ->
                onFetchRatesResult(outcome)
            }
        }
    }

    /**
     * Recalculates the 'to' and 'from' amounts if fetching rates was success
     * Notifies the UI and disables conversions if fetching was failure
     * @param outcome the outcome of fetching the rates from remote API
     */
    private fun onFetchRatesResult(outcome: Outcome<NetworkRateResponse>) {
        when (outcome) {
            is Outcome.Success -> {
                rates = outcome.value.toDomainModel()
                isRatesInSync = true
                recalculateExpectedAmount()
                _exchangeRates.postValue(rates.toCurrencyCodesList())
            }
            is Outcome.Failure -> {
                if (isRatesInSync) {
                    _statusEvent.postValue(ExchangerStatusEvent.Error(FetchRatesError.NO_CONNECTION))
                }
                isRatesInSync = false
            }
        }
    }

    /**
     * Updates the cached selected currencies values
     * Triggers the recalculation of the displayed amounts
     */
    fun updateCurrencySelection(currencyFrom: String, currencyTo: String) {
        userPreferences.toCurrency = currencyTo.trim()
        userPreferences.fromCurrency = currencyFrom.trim()
        recalculateExpectedAmount()
    }

    /**
     * Updates the cached from and to amounts value that the user types in
     * Triggers the recalculation of the displayed amounts
     * Displays 0 (zero) if one field is empty
     */
    fun updateField(field: SelectedField, value: String) {
        userPreferences.selectedField = field
        try {
            when (field) {
                SelectedField.From -> userPreferences.fromAmount = BigDecimal(value)
                SelectedField.To -> userPreferences.toAmount = BigDecimal(value)
            }
            recalculateExpectedAmount()
        } catch (e: NumberFormatException) {
            when (field) {
                SelectedField.From -> {
                    userPreferences.fromAmount = BigDecimal.ZERO
                    cacheAndNotifyToAmount(BigDecimal.ZERO)
                }
                SelectedField.To -> {
                    userPreferences.toAmount = BigDecimal.ZERO
                    cacheAndNotifyFromAmount(BigDecimal.ZERO)
                }
            }
        }
    }

    /**
     * Recalculates the 'to' or 'from' amounts based on which one was edited last
     * Notifies the UI about the changes
     */
    private fun recalculateExpectedAmount() {
        val rateFrom = rates.ratesMap[userPreferences.fromCurrency] ?: return
        val rateTo = rates.ratesMap[userPreferences.toCurrency] ?: return

        when (userPreferences.selectedField) {
            SelectedField.From -> {
                cacheAndNotifyToAmount(
                    exchangeAmountCalculator.calculateExpectedAmount(
                        userPreferences.fromAmount,
                        rateFrom,
                        rateTo
                    ).toRepresentationalScale()
                )
            }
            SelectedField.To -> {
                cacheAndNotifyFromAmount(
                    exchangeAmountCalculator.calculateRequiredAmount(
                        userPreferences.toAmount,
                        rateFrom,
                        rateTo
                    ).toRepresentationalScale()
                )
            }
            else -> { /* Do nothing */ }
        }
    }

    /**
     * Helper method to cache and change the 'to' amount in the UI
     */
    private fun cacheAndNotifyToAmount(amount: BigDecimal) {
        userPreferences.toAmount = amount
        _toAmount.value = amount
    }

    /**
     * Helper method to cache and change the 'from' amount in the UI
     */
    private fun cacheAndNotifyFromAmount(amount: BigDecimal) {
        userPreferences.fromAmount = amount
        _fromAmount.value = amount
    }

    /**
     * Performs the initial validation and proceeds exchanging
     * Triggered when the exchange button is pressed
     */
    fun exchange() {
        when {
            !isRatesInSync -> {
                _statusEvent.postValue(ExchangerStatusEvent.Error(FetchRatesError.OUT_OF_SYNC))
            }
            !ValidationUtils.validateUserPreferences(userPreferences) -> {
                _statusEvent.postValue(ExchangerStatusEvent.Error(ExchangeError.EMPTY_DATA))
            }
            !ValidationUtils.validateCurrency(userPreferences) -> {
                _statusEvent.postValue(ExchangerStatusEvent.Error(ExchangeError.CURRENCIES_MATCH))
            }
            else -> performConversion()
        }
    }

    /**
     * Performs the conversion
     * Calculates commission fee, checks if the balance is too low
     * Notifies the UI or saves the conversion
     */
    private fun performConversion() {
        viewModelScope.launch(Dispatchers.Default) {
            val commissionFee = exchangeAmountCalculator.calculateCommissionsFee(
                ConversionData(
                    exchangerHistoryRepository.getCount() + 1,
                    userPreferences.fromAmount,
                    exchangeAmountCalculator.calculateExpectedAmount(
                        userPreferences.fromAmount,
                        rates.ratesMap[userPreferences.fromCurrency] ?: error("Non-existing currency was provided"),
                        BigDecimal.ONE),
                    userPreferences.fromCurrency,
                    Date()
                )
            )

            if (ValidationUtils.isBalanceTooLow(balances[userPreferences.fromCurrency], userPreferences.fromAmount, commissionFee)) {
                _statusEvent.postValue(ExchangerStatusEvent.Error(ExchangeError.INSUFFICIENT_FUNDS))
            } else {
                saveExchange(
                    ExchangerStamp(
                        Date(),
                        userPreferences.fromAmount,
                        userPreferences.fromCurrency,
                        commissionFee,
                        userPreferences.toAmount,
                        userPreferences.toCurrency
                    )
                )
            }
        }
    }

    /**
     * Saves the calculated and validated data to the local database
     * @param stamp data about the conversion
     */
    private suspend fun saveExchange(
        stamp: ExchangerStamp
    ) {
        exchangerRepository.updateBalance(
            stamp.currencyFrom,
            CalculationsUtils.calculateNewBalanceSubtract(balances, stamp.currencyFrom, stamp.amountFrom.add(stamp.commissionAmount))
        )
        exchangerRepository.updateBalance(
            stamp.currencyTo,
            CalculationsUtils.calculateNewBalance(balances, stamp.currencyTo, stamp.amountTo)
        )

        exchangerHistoryRepository.saveExchange(stamp)

        _statusEvent.postValue(
            ExchangerStatusEvent.ExchangeSuccess(
                "${stamp.amountFrom.toRepresentationalScale()} ${stamp.currencyFrom}",
                "${stamp.amountTo.toRepresentationalScale()} ${stamp.currencyTo}",
                "${stamp.commissionAmount.toRepresentationalScale()} ${stamp.currencyFrom}"
            )
        )
    }
}

