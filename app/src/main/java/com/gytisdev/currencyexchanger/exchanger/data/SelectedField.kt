package com.gytisdev.currencyexchanger.exchanger.data

enum class SelectedField {
    None,
    From,
    To
}