package com.gytisdev.currencyexchanger.exchanger

import com.gytisdev.currencyexchanger.database.daos.HistoryDao
import com.gytisdev.currencyexchanger.exchanger.data.ExchangerStamp
import com.gytisdev.currencyexchanger.database.entities.HistoryEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * Repository responsible for actions related to manipulating conversion history
 */
class ExchangerHistoryRepository @Inject constructor(private val historyDao: HistoryDao) {
    /**
     * Save a new entry for a successful conversion into the local database
     * @param exchangeStamp data about the conversion
     */
    suspend fun saveExchange(exchangeStamp: ExchangerStamp) {
        withContext(Dispatchers.IO) {
            historyDao.insert(
                HistoryEntity(
                    date = exchangeStamp.date,
                    currencyFrom = exchangeStamp.currencyFrom,
                    amountFrom = exchangeStamp.amountFrom,
                    amountCommission = exchangeStamp.commissionAmount,
                    currencyTo = exchangeStamp.currencyTo,
                    amountTo = exchangeStamp.amountTo
                )
            )
        }
    }

    /**
     * Returns the number of entries in history table
     * @return the number of entries
     */
    suspend fun getCount() =
        withContext(Dispatchers.IO) {
            historyDao.getCount()
        }
}
