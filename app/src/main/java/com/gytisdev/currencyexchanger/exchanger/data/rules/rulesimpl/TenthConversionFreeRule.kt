package com.gytisdev.currencyexchanger.exchanger.data.rules.rulesimpl

import com.gytisdev.currencyexchanger.constants.CommissionsConstants
import com.gytisdev.currencyexchanger.exchanger.data.ConversionData
import com.gytisdev.currencyexchanger.exchanger.data.rules.Rule
import java.math.BigDecimal

class TenthConversionFreeRule : Rule {
    override fun calculateCommissionsFee(ruleData: ConversionData) : BigDecimal {
        if(ruleData.conversionCount % Constants.COUNT == 0) {
            return Constants.COMMISSION_FEE
        }

        return CommissionsConstants.DEFAULT_COMMISSION_FEE_PERCENT
    }

    private object Constants {
        const val COUNT = 10
        val COMMISSION_FEE: BigDecimal = BigDecimal.ZERO
    }
}