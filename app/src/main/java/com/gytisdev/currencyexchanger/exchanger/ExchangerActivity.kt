package com.gytisdev.currencyexchanger.exchanger

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.gytisdev.currencyexchanger.R
import com.gytisdev.currencyexchanger.databinding.ActivityExchangerBinding
import com.gytisdev.currencyexchanger.di.ViewModelFactory
import com.gytisdev.currencyexchanger.di.inject
import com.gytisdev.currencyexchanger.exchanger.data.BalanceAdapter
import com.gytisdev.currencyexchanger.exchanger.data.ExchangerStatusEvent
import com.gytisdev.currencyexchanger.exchanger.data.SelectedField
import com.gytisdev.currencyexchanger.extensions.observeOnce
import com.gytisdev.currencyexchanger.extensions.showAlertDialog
import com.gytisdev.currencyexchanger.exchanger.data.models.AlertDialogData
import javax.inject.Inject

class ExchangerActivity : AppCompatActivity() {

    @Inject
    lateinit var exchangerViewModelFactory: ViewModelFactory<ExchangerViewModel>
    private lateinit var exchangerViewModel: ExchangerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        val binding: ActivityExchangerBinding = DataBindingUtil.setContentView(this,
            R.layout.activity_exchanger
        )

        setupViewModels()
        setupViews(binding)
        setupObservers(binding)

        binding.lifecycleOwner = this
        binding.viewModel = exchangerViewModel

        exchangerViewModel.initialize()
    }

    private fun setupObservers(binding: ActivityExchangerBinding) {
        exchangerViewModel.statusEvent.observe(this, Observer { statusEvent ->
            when(statusEvent) {
                is ExchangerStatusEvent.ExchangeSuccess -> {
                    showAlertDialog(AlertDialogData(
                        title = getString(R.string.title_success_exchange),
                        message = getString(
                            R.string.message_success_exchange,
                            statusEvent.from,
                            statusEvent.to,
                            statusEvent.commissionFee),
                        buttonText = getString(android.R.string.ok)
                    ))
                }
                is ExchangerStatusEvent.Error -> {
                    showAlertDialog(AlertDialogData(
                        title = getString(R.string.title_error),
                        message = getString(statusEvent.error.messageId()),
                        buttonText = getString(android.R.string.ok)
                    ))
                }
            }
        })
        exchangerViewModel.exchangeRates.observeOnce(this, Observer { rates ->
            binding.spnCurrencies.adapter = ArrayAdapter(applicationContext, R.layout.item_spinner_currency, rates)
            binding.spnCurrenciesTo.adapter = ArrayAdapter(applicationContext, R.layout.item_spinner_currency, rates)
        })
    }

    private fun setupViews(binding: ActivityExchangerBinding){
        val onSpinnerItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                exchangerViewModel.updateCurrencySelection(
                    binding.spnCurrencies.selectedItem.toString(),
                    binding.spnCurrenciesTo.selectedItem.toString()
                )
            }
        }
        binding.etRequiredAmount.doOnTextChanged { text, _, _, _ ->
            if(binding.etRequiredAmount.isFocused) {
                exchangerViewModel.updateField(SelectedField.From, text.toString())
            }
        }
        binding.etExpectedAmount.doOnTextChanged { text, _, _, _ ->
            if(binding.etExpectedAmount.isFocused) {
                exchangerViewModel.updateField(SelectedField.To, text.toString())
            }
        }
        binding.spnCurrencies.onItemSelectedListener = onSpinnerItemSelectedListener
        binding.spnCurrenciesTo.onItemSelectedListener = onSpinnerItemSelectedListener
        binding.listBalances.adapter = BalanceAdapter()
    }

    private fun setupViewModels() {
        exchangerViewModel = ViewModelProvider(this, exchangerViewModelFactory)[ExchangerViewModel::class.java]
    }

}
