package com.gytisdev.currencyexchanger.exchanger.data.rules

import com.gytisdev.currencyexchanger.exchanger.data.rules.rulesimpl.DefaultRule
import com.gytisdev.currencyexchanger.exchanger.data.rules.rulesimpl.FirstFiveFreeRule
import com.gytisdev.currencyexchanger.exchanger.data.rules.rulesimpl.TenthConversionFreeRule
import javax.inject.Inject

class RulesProvider @Inject constructor() {
    val activeRules = listOf(
        DefaultRule(),
        TenthConversionFreeRule(),
        FirstFiveFreeRule()
    )
}