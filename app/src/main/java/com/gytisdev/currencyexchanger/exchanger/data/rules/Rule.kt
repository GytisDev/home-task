package com.gytisdev.currencyexchanger.exchanger.data.rules

import com.gytisdev.currencyexchanger.exchanger.data.ConversionData
import java.math.BigDecimal

interface Rule {
    fun calculateCommissionsFee(ruleData: ConversionData) : BigDecimal
}