package com.gytisdev.currencyexchanger.exchanger.data

import java.math.BigDecimal
import java.util.*

data class ExchangerStamp (
    val date: Date,
    val amountFrom: BigDecimal,
    val currencyFrom: String,
    val commissionAmount: BigDecimal,
    val amountTo: BigDecimal,
    val currencyTo: String
)