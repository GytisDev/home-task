package com.gytisdev.currencyexchanger.exchanger.data.rules.rulesimpl

import com.gytisdev.currencyexchanger.constants.CommissionsConstants
import com.gytisdev.currencyexchanger.exchanger.data.ConversionData
import com.gytisdev.currencyexchanger.exchanger.data.rules.Rule
import java.math.BigDecimal

class UnderBaseAmountFree : Rule {
    override fun calculateCommissionsFee(ruleData: ConversionData): BigDecimal {
        if(ruleData.conversionAmountInBase < Constants.AMOUNT) {
            return BigDecimal.ZERO
        }

        return CommissionsConstants.DEFAULT_COMMISSION_FEE_PERCENT
    }

    private object Constants {
        val AMOUNT = BigDecimal(200)
    }
}