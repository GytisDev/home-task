package com.gytisdev.currencyexchanger.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.math.BigDecimal
import java.util.*

/**
 * Database entity that holds conversion history entry
 */
@Entity(tableName = "history")
data class HistoryEntity (
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val date: Date,
    val currencyFrom: String,
    val amountFrom: BigDecimal,
    val amountCommission: BigDecimal,
    val currencyTo: String,
    val amountTo: BigDecimal
)