package com.gytisdev.currencyexchanger.database.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gytisdev.currencyexchanger.database.entities.BalanceEntity

/**
 * Balance-relate data access object that enables to manipulate data at the database
 */
@Dao
interface BalanceDao {
    /**
     * Inserts the provided [BalanceEntity] into the database. Replaces if one already exists
     * @param balanceEntity entity that will be inserted into the database
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBalance(balanceEntity: BalanceEntity)

    /**
     * Provides [LiveData] to observe the balance from UI
     * @return live data object observing the balance table changes
     */
    @Query("SELECT * FROM BALANCE")
    fun getBalance() : LiveData<List<BalanceEntity>>

    /**
     * Updates a record in the balance table where currency is matched
     * @param currency currency that will be updated
     * @param amount amount that will be set to specified currency
     */
    @Query("UPDATE balance SET amount=:amount WHERE currency=:currency")
    suspend fun updateBalance(currency: String, amount: String)

    /**
     * Removes currency from the database
     * @param currency currency that will be removed if matched
     */
    @Query("DELETE FROM balance WHERE currency=:currency")
    suspend fun deleteByCurrency(currency: String)
}