package com.gytisdev.currencyexchanger.database

import androidx.room.TypeConverter
import java.math.BigDecimal
import java.util.*

object Converters {
    @TypeConverter
    @JvmStatic
    fun bigDecimalToString(input: BigDecimal): String =
        input.toPlainString()

    @TypeConverter
    @JvmStatic
    fun stringToBigDecimal(input: String) =
        when {
            input.isBlank() -> BigDecimal(0)
            else -> input.toBigDecimalOrNull() ?: BigDecimal(0)
        }

    @TypeConverter
    @JvmStatic
    fun fromTimestamp(value: Long): Date {
        return Date(value);
    }

    @TypeConverter
    @JvmStatic
    fun dateToTimestamp(date: Date) : Long {
        return date.time
    }
}