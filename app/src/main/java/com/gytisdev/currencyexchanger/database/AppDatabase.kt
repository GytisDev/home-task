package com.gytisdev.currencyexchanger.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.gytisdev.currencyexchanger.database.entities.BalanceEntity
import com.gytisdev.currencyexchanger.database.daos.BalanceDao
import com.gytisdev.currencyexchanger.database.daos.HistoryDao
import com.gytisdev.currencyexchanger.database.entities.HistoryEntity
import com.gytisdev.currencyexchanger.utils.InitialisationBalanceReader
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@Database(
    entities = [BalanceEntity::class, HistoryEntity::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract val balanceDao: BalanceDao
    abstract val historyDao: HistoryDao

    companion object {
        private const val DATABASE_NAME = "app_db.db"
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it; }
            }

        private fun buildDatabase(context: Context) : AppDatabase {
            return Room.databaseBuilder(context.applicationContext,
                AppDatabase::class.java, DATABASE_NAME)
                .addCallback(object : Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        GlobalScope.launch {
                            prepopulateDatabase(context)
                        }
                    }
                })
                .build()
        }

        /**
         * Prepopulates the database after it's created for the first time
         * Reads data from JSON file
         */
        private suspend fun prepopulateDatabase(context: Context) {
            withContext(Dispatchers.IO) {
                val balances = InitialisationBalanceReader.readJsonInitialisationFile(context)
                val db = getInstance(context)
                balances.forEach {
                    db.balanceDao.insertBalance(it)
                }
            }
        }
    }

}