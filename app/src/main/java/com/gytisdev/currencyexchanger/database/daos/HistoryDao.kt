package com.gytisdev.currencyexchanger.database.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gytisdev.currencyexchanger.database.entities.HistoryEntity

/**
 * History-relate data access object that enables to manipulate data at the database
 */
@Dao
interface HistoryDao {
    /**
     * Inserts the provided [HistoryEntity] into the database
     * @param historyEntity the entry that will be saved
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(historyEntity: HistoryEntity)

    /**
     * Counts the number of hisotry entries in the database
     * @return number of entries in the database
     */
    @Query("SELECT COUNT(*) FROM history")
    suspend fun getCount() : Int

}