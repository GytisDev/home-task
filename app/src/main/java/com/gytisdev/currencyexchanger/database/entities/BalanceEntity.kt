package com.gytisdev.currencyexchanger.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.gytisdev.currencyexchanger.exchanger.data.models.Balance
import java.math.BigDecimal

/**
 * Balance entity that holds single currency information in database
 */
@Entity(tableName = "balance")
data class BalanceEntity (
    @PrimaryKey(autoGenerate = false)
    val currency: String,
    val amount: BigDecimal
)

fun List<BalanceEntity>.toDomainModel() =
    this.map {
        Balance(
            currency = it.currency,
            amount = it.amount
        )
    }